all: build

include src/src.mk

.PHONY: all build build-manual build-snippets build-manual-html build-manual-epub force clean

#build: build-src build-manual
build: build-manual

build-snippets:
	mkdir -p en-US/snippets
	python scripts/split-snippets.py . \
	  src/*.c src/*.cpp src/*.java src/*.py src/*.go src/*.sh

build-manual: build-snippets
	rm -rf html && mkdir html
	cd en-US && asciidoctor -n master.adoc -D ../html && cp -ar Common_Content/ ../html/

build-manual-html: build-manual

build-manual-epub: build-snippets
	false

build-manual-pdf: build-snippets
	false

clean: clean-src
	-rm -rf html
	-rm -rf en-US/*/snippets

