/* Example code to illustrate PKI crypto ops (encrypt with public key,
 * decrypt with private key)
 *
 * Code assumes that you have set up a NSS database with a certificate
 * and a private key. 
 * Here is one way of doing it:
 *   # create CA cert db, if -f not provided, prompts for passwd
 *   $ certutil -N -d .
 *
 *   # load your hsm (optional) To do so you need to know where your
 *   # pkcs #11 module lives. replace {path_to_pkcs11_library} with
 *   # the actual path and library name (example /usr/lib64/libcoolkeypk11.so
 *   $ modultil -add "HSM Module" -libfile {path_to_pkcs11_library} -dbdir .
 *
 *   # create CA cert, self-signed, generates key-pair, prompts for key
 *   # type, cert type etc
 *   # {token_name} is the name of your PKCS #11 token. You can find
 *   #  the token name using 'modutil --list -dbdir .'. If you are using
 *   #  softoken you can skip the -h {token_name}
 *   # answers for prompts: 5,9,n,y,-1,n,5,6,7,9,n
 *   $ certutil -S -s -h {token_name} \
 *     "CN=Test CA, O=BOGUS Inc, L=Mtn View, ST=CA, C=US"  \
 *     -n TestCA -t CTu,CTu,CTu -v 60 -x -d . -1 -2 -5
 *
 *   Run the program with "{token_name}:TestCA" as the command line argument.
 *   You will be prompted for the pin.
 *
 * There are many ways to setup a public/private key to use - this
 * example shows one of them.
 *
 * This example does not do any padding. It simply encrypts/decrypts a block
 * of length equal to modulus length of the public/private key.
 */

#include <stdio.h>
#include <unistd.h>
#include <prerror.h>
#include <nss.h>
#include <nssutil.h>
#include <cert.h>
#include <cryptohi.h>
#include <keyhi.h>
#include <pk11pub.h>
#include <secoidt.h>

static char *pin = NULL;
/* this callback is responsible for returning the password to the NSS
 * key database. for example purposes, this function hardcodes the password.
 * In a real app, this function should obtain the password using secure means
 * such as prompting an operator, or retrieving it over a secure communication
 * channel
 */
char *passwdcb(PK11SlotInfo * info, PRBool retry, void *arg);

int main(int argc, char **argv)
{
  //+ Features HSM-NSS
  SECStatus rv;
  CERTCertificate *cert = NULL;
  SECKEYPrivateKey *pvtkey = NULL;
  SECItem signature = { siBuffer, NULL, 0 };
  SECOidTag algTag;
  int r = 1;
  unsigned char buf[] = "test data to sign";
  const char *cert_name;
  unsigned i;

  if (argc < 3) {
    fprintf(stderr, "usage: %s [cert name] [PIN]\n\n", argv[0]);
    exit(1);
  }

  cert_name = argv[1];
  pin = argv[2];

  PK11_SetPasswordFunc(passwdcb);
  NSS_InitializePRErrorTable();
  rv = NSS_Init(".");
  if (rv != SECSuccess) {
    fprintf(stderr, "NSS initialization failed (err %d)\n", PR_GetError());
    goto cleanup;
  }

  cert = PK11_FindCertFromNickname(cert_name, NULL);
  if (cert == NULL) {
    fprintf(stderr, "Couldn't find cert %s in NSS db (err %d: %s)\n",
	    cert_name, PR_GetError(), PORT_ErrorToString(PR_GetError()));
    goto cleanup;
  }

  fprintf(stderr, "Buffer being signed = \n%s\n", buf);

  pvtkey = PK11_FindKeyByAnyCert(cert, NULL);
  if (pvtkey == NULL) {
    fprintf(stderr, "Couldn't find private key for cert %s (err %d: %s)\n",
	    cert_name, PR_GetError(), PORT_ErrorToString(PR_GetError()));
    goto cleanup;
  }

  /* get the algtag. Pick the default hash algorithm */
  algTag = SEC_GetSignatureAlgorithmOidTag(pvtkey->keyType, SEC_OID_UNKNOWN);

  fprintf(stderr, "Signing with alg = %s (%d)\n",
	  SECOID_FindOIDTagDescription(algTag), algTag);

  rv = SEC_SignData(&signature, buf, sizeof(buf)-1, pvtkey, algTag);
  if (rv != SECSuccess) {
    fprintf(stderr, "sign with Private Key failed (err %d: %s)\n",
	    PR_GetError(), PORT_ErrorToString(PR_GetError()));
    goto cleanup;
  }
  //-

  fprintf(stderr, "Signature len = %d\n", signature.len);
  fprintf(stderr, "Signature data = ");
  /* dump signature.data */
  for (i = 0; i < signature.len; i++) {
    if ((i & 0xf) == 0)
      printf("\n");
    printf("%02x ", signature.data[i]);
  }
  printf("\n");

  r = 0;

 cleanup:
  if (cert)
    CERT_DestroyCertificate(cert);
  if (pvtkey)
    SECKEY_DestroyPrivateKey(pvtkey);
  if (signature.data)
    SECITEM_FreeItem(&signature, PR_FALSE);
  exit(r);
}

//+ Features HSM-NSS-PIN
char *passwdcb(PK11SlotInfo * slot, PRBool retry, void *arg)
{
  if (!isatty(STDIN_FILENO) && retry) {
    /* we're just reading from a file, and the value is known to be wrong,
     * don't keep bounding the token with the wrong password. */
    return NULL;
  }

  if (retry) {
    printf("Warning: Wrong PIN has been provided in the previous attempt\n");
    if (PK11_IsHW(slot)) {
      printf
	  ("  NOTE: multiple pin failures could result in locking your device\n");
    }
  }

  if (pin == NULL)
    return pin;
  else
    return strdup(pin);
}
//-
