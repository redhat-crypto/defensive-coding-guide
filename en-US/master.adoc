:doctype: book
:toc: left
:toclevels: 3
:source-highlighter: pygments
:pygments-style: friendly
:pygments-linenums-mode: inline

= Defensive Coding Guide

include::Book_Info.adoc[]

== Programming Languages

include::C.adoc[]

include::CXX.adoc[]

include::Java.adoc[]

include::Python.adoc[]

include::Shell.adoc[]

include::Go.adoc[]

include::Vala.adoc[]

== Specific Programming Tasks

include::Tasks-Library_Design.adoc[]

include::Tasks-Descriptors.adoc[]

include::Tasks-File_System.adoc[]

include::Tasks-Temporary_Files.adoc[]

include::Tasks-Processes.adoc[]

include::Tasks-Serialization.adoc[]

include::Tasks-Cryptography.adoc[]

include::Tasks-Packaging.adoc[]

== Implementing Security Features

include::Features-Authentication.adoc[]

include::Features-TLS.adoc[]

include::Features-HSM.adoc[]

include::Revision_History.adoc[]
