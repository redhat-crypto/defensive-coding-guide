# Security coding guide

This is a fork of the Fedora secure coding guide.

The guide is present in HTML form (auto-generated from this
repository) at:

 * http://redhat-crypto.gitlab.io/defensive-coding-guide


# Building HTML documentation

Just type "make".  If you do not want to build the example code, run
"make build-manual".

When you type "make", the code examples in src/ are compiled (mainly
to check for obvious syntax errors, but also for manual testing).  If
you lack the necessary libraries, you can type "make build-manual"
instead, which will skip this step.  The code examples are still
included in the manual.


# Dependencies

Building the manual pages needs the "publican" and the "publican-fedora"
packages.