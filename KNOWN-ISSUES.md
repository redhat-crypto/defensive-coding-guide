# Known issues

 * No mention of rust
 * Enhance sections on serialization with examples of protocol buffers
 * No mention of process isolation mechanisms/sandboxing (seccomp)
 * No mention of programming with SELinux for safety

